from django.db import models

# Create your models here.
class Marca(models.Model):
    nombre_marca = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre_marca



class Auto(models.Model):
    marca_auto = models.ForeignKey(Marca, on_delete=models.PROTECT)
    modelo = models.CharField(max_length=50)
    precio = models.IntegerField()
    cantidad = models.IntegerField()

    def __str__(self):
        return self.modelo
