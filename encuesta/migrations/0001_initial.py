# Generated by Django 4.2.4 on 2024-05-26 22:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_marca', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Auto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modelo', models.CharField(max_length=50)),
                ('precio', models.IntegerField()),
                ('cantidad', models.IntegerField()),
                ('marca_auto', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='encuesta.marca')),
            ],
        ),
    ]
